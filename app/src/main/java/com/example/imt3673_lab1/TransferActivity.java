package com.example.imt3673_lab1;

import android.content.Intent;
import android.os.Bundle;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Timestamp;
import java.time.Clock;

public class TransferActivity extends AppCompatActivity {
    //Spinner spinner = (Spinner) findViewById(R.id.spn_recipients);
    float balance;
   public float f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        final Spinner spinner = (Spinner) findViewById(R.id.spn_recipients);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.spn_devils, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        Intent intent = getIntent();
        balance = intent.getFloatExtra("balance", 0.f);

        final Button button = findViewById(R.id.btn_pay);
        button.setEnabled(false);
        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent back = new Intent();
                back.putExtra("recipient",spinner.getSelectedItem().toString());
                back.putExtra("amount",f);
                back.putExtra("timeStamp",new Long(System.currentTimeMillis()));
                setResult(200,back);
                finish();
            }
        });

        EditText amount = findViewById(R.id.txt_amount);

        amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Fires right as the text is being changed (even supplies the range of text)
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // Fires right before text is changing
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Fires right after the text has changed
                f = Float.valueOf(s.toString());
                if (f < balance && f != 0 && spinner.getSelectedItemPosition() != 0) {
                    button.setEnabled(true);
                }
            }

        });

    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        setResult(400);
        finish();
    }
}

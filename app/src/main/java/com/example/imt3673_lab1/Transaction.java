package com.example.imt3673_lab1;

import java.sql.Timestamp;
import java.time.Instant;

public class Transaction {
    String recipient;
    Float amount;
    Timestamp timeStamp;
    public Transaction(String recipient, Float amount, Timestamp timeStamp) {
        this.recipient = recipient;
        this.amount = amount;
        this.timeStamp = timeStamp;
    }
}

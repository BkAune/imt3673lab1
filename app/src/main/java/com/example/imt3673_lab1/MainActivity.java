package com.example.imt3673_lab1;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {

    int balanceDisplay;
    float balance;
    TextView balanceText;
    ArrayList<Transaction> transactions = new ArrayList<Transaction>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        balanceText = (TextView)findViewById(R.id.lbl_balance);
        balance = random_int(90,110);
        transactions.add(new Transaction("God", balance, new Timestamp(System.currentTimeMillis())));
        balanceDisplay = (int)balance;
        balanceText.setText(Integer.toString(balanceDisplay));
        final Button transactionButton = findViewById(R.id.btn_transaction);
        transactionButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent transactionIntent = new Intent(getApplicationContext(), TransactionActivity.class);
                transactionIntent.putExtra("amount", transactions);
                startActivity(transactionIntent);
            }
        });

        Button transferButton = findViewById(R.id.btn_transfer);
        transferButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Intent transferIntent =  new Intent(getApplicationContext(),TransferActivity.class);
                transferIntent.putExtra("balance",balance);
                startActivityForResult(transferIntent,1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            if (resultCode == 200){
                balance = balance - data.getFloatExtra("amount", 0.f);
                balanceDisplay = (int)balance;
                balanceText.setText(Integer.toString(balanceDisplay));
                transactions.add(new Transaction(data.getStringExtra("recipient"),(data.getFloatExtra("amount",0.f)*-1), new Timestamp(data.getLongExtra("timeStamp", 0))));
            } else if (resultCode == 400){

            }
        }
    }

    public static int random_int(int Min, int Max)
    {
        return (int) (Math.random()*(Max-Min))+Min;
    }
}
